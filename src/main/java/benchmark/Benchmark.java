package benchmark;

import modele.Edt;
import resolution.Metah;
import solution.KPI;
import solution.SolutionChecker;
import modele.Main;
import modele.WsrModel;
import resolution.Heuristique;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.List;

public class Benchmark {


    public static List<Edt> glutton(WsrModel model) {
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();
        return solverGlouton.getSolution();
    }

    public static List<Edt> meta(WsrModel model) {
        Metah meta = new Metah(model);
        meta.solve();
        return meta.getSolution();
    }

    /**
    public static List<Edt> mip(WsrModel model) {
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();
        return solverGlouton.getSolution();
    }**/


    public static void timeBench() throws FileNotFoundException {
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        String pathresult="files/ResultatGloutton.txt";
        String resultat="";
        WsrModel model = null;
        long cpuTime;
        long startCpuTime;
        List<Edt> sol = null;
        KPI kpi =  new KPI();

        for (int i = 20; i < 100; i+=5) {
            model = new WsrModel(Main.InstanceGeneratorJobs((int) (Math.log(i)*i),1),Main.InstanceGeneratorEmployee(i,2));
            glutton(model);
        }// Chauffe de la JVM

        for (int i = 10; i < 700; i+=10) {// On itere sur la taille des instances

            startCpuTime = thread.getCurrentThreadCpuTime();

            for (int j = 0; j < 5 ; j++) {//Pour eviter les resultats extremes
                model = new WsrModel(Main.InstanceGeneratorJobs((int) (Math.log(i)*i),0),Main.InstanceGeneratorEmployee(i,2));
                for (int k = 0; k < 5; k++) {//Pour eviter un ralentissement materiel
                    try {
                        sol = meta(model);
                    } catch (Exception e) {
                        //System.out.println("Erreur ");
                    }

                }
            }

            cpuTime =(thread.getCurrentThreadCpuTime() - startCpuTime)/1000;//MicroSECONDS
            System.out.println(i);
            //System.out.println("Iteration = "+i+"    Temps ="+(float)cpuTime/1000+"us\n"+
              //      " Ratio Affecté = "+kpi.numbersJobSatisfyOnAllJob(sol,model)+
                //
            //    " \nRatio Voulou = "+kpi.numbersTimeJobSatisfyOnAllJob(sol));
            resultat+=i+" "+cpuTime+
                    //" "+kpi.averageTravelTimeByEmploye(sol)+
                    //" "+kpi.averageTravelTimeByJob(sol)+
                    //" "+kpi.effectiveWorkTimeOnPossibleWorkTime(sol)+
                    //" "+kpi.numbersEmployeWhoWorkOnAllEmploye(sol,model)+
                    " "+kpi.numbersJobSatisfyOnAllJob(sol,model)+
                    " "+kpi.numbersTimeJobSatisfyOnAllJob(sol)+
                    "\n";

        }

        PrintWriter writer = new PrintWriter(pathresult);
        writer.print(resultat);
        writer.close();
    }
    public static void main(String[] args) throws FileNotFoundException {
        timeBench();
    }
}
