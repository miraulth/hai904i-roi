package solution;

import modele.Edt;
import modele.WsrModel;

import java.util.List;

public class KPI {

    public KPI() {
    }

    public int averageTravelTimeByJob(List<Edt> solution)
    {
        int timetot = 0;
        int traveltot = 0;
        for (int i = 0; i < solution.size(); i++){
            for (int j = 0; j < solution.get(i).getTimeTravel().size(); j++) {
                timetot += solution.get(i).getTimeTravel().get(j).time();
                traveltot++;
            }
        }
        if (traveltot==0) return 0;
        return timetot/traveltot;
    }
    public int averageTravelTimeByEmploye(List<Edt> solution)
    {
        int timetot = 0;
        for (int i = 0; i < solution.size(); i++){
            for (int j = 0; j < solution.get(i).getTimeTravel().size(); j++) {
                timetot += solution.get(i).getTimeTravel().get(j).time();
            }
        }
        if (solution.isEmpty()) return 0;
        return timetot/solution.size();
    }

    public float effectiveWorkTimeOnPossibleWorkTime(List<Edt> solution)
    {
        int timeWork = 0;
        int possibleTimeWork = 0;
        for (int i = 0; i < solution.size(); i++){
            for (int j = 0; j < solution.get(i).getAffectation().size(); j++) {
                timeWork += solution.get(i).getAffectation().get(j).getHoraire().time();
            }
        }
        for (int i = 0; i < solution.size(); i++){
            for (int j = 0; j < solution.get(i).getEmploye().getHoraires().size(); j++) {
                possibleTimeWork += solution.get(i).getEmploye().getHoraires().get(j).time();
            }
        }

        if (possibleTimeWork==0) return 0;
        return (float)timeWork/possibleTimeWork;
    }

    public float numbersJobSatisfyOnAllJob(List<Edt> solution, WsrModel model)
    {// TODO, corriger, ne tourne pas
        int jobSatisfy = 0;
        for (int i = 0; i < solution.size(); i++){
            if(solution.get(i).getAffectation() != null) {
                jobSatisfy += solution.get(i).getAffectation().size();
                }
            }
        return (float)jobSatisfy/model.getJobs().size();
    }


    public float numbersTimeJobSatisfyOnAllJob(List<Edt> solution)
    {
        float job = 0;
        float jobSatisfy = 0;
        for (Edt edt : solution) {
            if (edt.getAffectation() != null) {
                job += edt.getAffectation().size();
                for (int j = 0; j < edt.getAffectation().size(); j++) {
                    if (edt.getAffectation().get(j).getHoraire().getBeg().isAfter(edt.getAffectation().get(j).getTravail().getHorairevoulu().getBeg()) && edt.getAffectation().get(j).getHoraire().getEnd().isBefore(edt.getAffectation().get(j).getTravail().getHorairevoulu().getEnd()))
                    {
                        jobSatisfy++;
                    }
                }
            }
        }
        //System.out.println(jobSatisfy);
        if (job==0) return 0;
        return jobSatisfy/job;
    }
}