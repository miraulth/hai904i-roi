package solution;

import modele.Edt;
import modele.Job;
import modele.WsrModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SolutionChecker {



    public static boolean isSolution(List<Edt> solution, WsrModel model)
    {

        Map<Job,Integer> listJob = new HashMap<Job,Integer>();
        for(int i=0;i < model.getJobs().size()-1;i++){
            listJob.put(model.getJobs().get(i),0);
        }
        boolean isSol = true;
        int i = 0;

        while((i < solution.size())&&(isSol))
        {
            //skills
            isSol = solution.get(i).edtIsClear();
            if(!isSol){
                System.out.println("Chevauchement sur l'emploie du temps "+i);
            }
            int j = 0;
            while((j < solution.get(i).getAffectation().size())&&(isSol)){
                listJob.replace(solution.get(i).getAffectation().get(j).getTravail(),listJob.get(solution.get(i).getAffectation().get(j).getTravail()),listJob.get(solution.get(i).getAffectation().get(j).getTravail())+1);
                if(listJob.get(solution.get(i).getAffectation().get(j).getTravail())>1){
                    isSol = false;
                    System.out.println("Job"+solution.get(i).getAffectation().get(j).getTravail()+" affécté 2 fois");

                }
                isSol= isSol && solution.get(i).getEmploye().getAbL().get(0).compatibleSkills(solution.get(i).getAffectation().get(j).getTravail().getAbL(),solution.get(i).getEmploye().getAbL());
                if(!isSol){
                    System.out.println("Le travailleur "+solution.get(i).getEmploye()+" n'a pas les compétence pour le job "+solution.get(i).getAffectation().get(j).getTravail());
                }
                j++;
            }
            i++;
        }
        return isSol;
    }
}