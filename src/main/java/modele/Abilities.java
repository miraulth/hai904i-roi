package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Abilities {
    private final String skill;
    private int level;

    @JsonCreator
    public Abilities(@JsonProperty("level") int l,@JsonProperty("skill") String s) {
        this.skill = s;
        this.level = l;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSkill() {
        return skill;
    }


    public boolean compatibleSkill(Abilities a) {return (a.getSkill().equals(this.skill) && a.getLevel() >= this.level);}

    public boolean compatibleSkills(List<Abilities> abLToVerify,List<Abilities> abL) {
        boolean isCompatible = true;
        for(int i = 0;i<abLToVerify.size();i++){
            int j = 0;
            boolean tempCompatible = false;
            while((j<abL.size())&&(!tempCompatible)){
                if(abLToVerify.get(i).compatibleSkill(abL.get(j))){
                    tempCompatible = true;
                }
                j++;
            }
            isCompatible = isCompatible && tempCompatible;

        }
        return isCompatible;
    }
    @Override
    public String toString() {
        return  "skill = " + skill ;
    }
}
