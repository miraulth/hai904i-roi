package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class WsrModel {
    private List<Job> jobs;
    private List<Employee> lemploye;

    @JsonCreator
    public WsrModel(@JsonProperty("jobs") List<Job> jobs,@JsonProperty("lemploye") List<Employee> lemploye) {
        this.jobs = jobs;
        this.lemploye = lemploye;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }


    public List<Employee> getLemploye() {
        return lemploye;
    }

    public void setLemploye(List<Employee> lemploye) {
        this.lemploye = lemploye;
    }

    public void addJob(Job j) {
        this.jobs.add(j);
    }

    public void removeJob(Job j) {
        this.jobs.remove(j);
    }

    public void addLemploye(Employee e) {
        this.lemploye.add(e);
    }

    public void removeLemploye(Employee e) {
        this.lemploye.remove(e);
    }

    @Override
    public String toString() {
        return "WsrModel{" +
                "\njobs=" + jobs +
                ", \nlemploye=" + lemploye +
                '}';
    }
}

