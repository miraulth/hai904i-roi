package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class TimeInterval {
    private Instant beg;
    private Instant end;
@JsonCreator
    public TimeInterval(@JsonProperty("beg") Instant beg,@JsonProperty("end") Instant end){
        this.beg = beg;
        this.end = end;
    }

    public TimeInterval(Instant beg,int time){
        this.beg = beg;
        this.end = beg.plusSeconds(time);
    }

    public TimeInterval(int time,Instant end){
        this.beg = end.minusSeconds(time);
        this.end = end;
    }

    public Instant getBeg() {
        return beg;
    }

    public void setBeg(Instant beg) {
        this.beg = beg;
    }

    public Instant getEnd() {
        return end;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    public int time()
    {
        return (int)(this.getEnd().getEpochSecond() - this.getBeg().getEpochSecond());
    }

    public boolean includeIn( TimeInterval i)
    {
        return (this.getBeg().isBefore(i.getBeg()) && (this.getEnd().isAfter(i.getEnd())));
    }



    public String toString()
    {
        return "[ "+this.getBeg()+" : "+this.getEnd()+"]";
    }


}
