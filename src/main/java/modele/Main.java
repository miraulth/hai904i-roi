package modele;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    private static Random r = new Random();




    public static Location InstanceGeneratorLocation(int id){
        /*
         * Point NO :43.563972, 4.085444 = La Grande Motte
         *            N/S          E/O
         *   1latitude = 111km   1longitude = 80km (en France)
         * Point SE :43.912559, 3.512650  = Blandas
         * */

        float xsup = (float) 43.912559;
        float xinf = (float) 43.563972;
        float ysup = (float) 4.085444;
        float yinf = (float) 3.512650;
        float x = xinf+(r.nextFloat()*(xsup - xinf));
        float y = yinf+(r.nextFloat()*(ysup - yinf));
        return new Location(id,x,y);
    }
    public static Abilities InstanceGeneratorAbilities() {
        int value = r.nextInt(Skill.values().length);
        Abilities abi = new Abilities(1+r.nextInt(5),Skill.values()[value].toString());
        return abi;
    }
    public static List<Job> InstanceGeneratorJobs(int nbjob,int nbcompetence) {

        List<Job> joblist = new ArrayList<Job>();
        Instant depart = Instant.now().truncatedTo(ChronoUnit.DAYS);
        /**
         * 86400 secondes dans une journée
         * 82800 secondes dans 23H
         * 1440 minutes dans une journée
         * **/

        for (int i = 0; i < nbjob ; i++) {
            Location loc = InstanceGeneratorLocation(i);
            List<Abilities> competences = new ArrayList<Abilities>();
            for (int j = 0; j < nbcompetence; j++) {
                competences.add(InstanceGeneratorAbilities());
            }

            Instant i1n = depart.plusSeconds(r.nextInt(72000));
            Instant i1v = i1n.plusSeconds((long)1800+r.nextInt(1800));
            Instant i2v = i1v.plusSeconds((long)1800+r.nextInt(1800));
            Instant i2n = i2v.plusSeconds((long)1800+r.nextInt(1800));
            TimeInterval daten = new TimeInterval(i1n,i2n);
            TimeInterval datev = new TimeInterval(i1v,i2v);
            long duree = (long)r.nextInt(1800);
            Job j = new Job(i,loc,competences,datev,daten,duree);
            joblist.add(j);
        }
        return joblist;
    }
    public static List<Employee> InstanceGeneratorEmployee(int nbEmployee,int nbAbilities) {
        List<Employee> employeelist = new ArrayList<Employee>();
        for(int i = 0 ; i < nbEmployee ; i ++) {
            Location locdepart = InstanceGeneratorLocation(i);
            //Location locarrive = InstanceGeneratorLocation(i+nbEmployee);
            Location locarrive= locdepart;
            List<TimeInterval> horaires = new ArrayList<TimeInterval>();
            Instant depart = Instant.now().truncatedTo(ChronoUnit.DAYS);
            Instant arivee;
            TimeInterval date;
            for (int j = 0; j < 2; j++) {
                arivee = depart.plusSeconds(3600).plusSeconds(r.nextInt(7200));
                date =  new TimeInterval(depart,arivee);
                horaires.add(date);
                depart = arivee.plusSeconds(3600);

            }

            date = new TimeInterval(depart,Instant.now().truncatedTo(ChronoUnit.DAYS).plusSeconds(32400));
            horaires.add(date);
            List<Abilities> abL = new ArrayList<Abilities>();
            for (int j = 0; j < nbAbilities; j++) {
                abL.add(InstanceGeneratorAbilities());
            }
            Employee e = new Employee(i,locdepart,locarrive,abL,horaires);
            employeelist.add(e);
        }
        return  employeelist;
    }

    public static void writeJsonModel(WsrModel model,File file) throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, model);

    }

    public static WsrModel readJsonModel(File file) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        WsrModel model = mapper.readValue(file, WsrModel.class);
        return model;
    }

    public static void writeJsonEdt(Edt edt,File file) throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, edt);

    }

    public static Edt readJsonEdt(File file) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        Edt edt = mapper.readValue(file, Edt.class);
        return edt;
    }

    public static void main(String args[]) throws IOException {


        List<Job> joblist;
        List<Employee> employeelist;
        WsrModel model;
        File file;

        for (int i = 10; i < 101; i+= 10) {
            joblist = InstanceGeneratorJobs(3*i,1);
            employeelist = InstanceGeneratorEmployee(i,3);
            model = new WsrModel(joblist,employeelist);
            file = new File("files/Instance_"+i+".json");
            writeJsonModel(model, file);

        }
        /**
        File file2 = new File("files/Modele.Edt.json");
        writeJsonEdt(edt, file2);
         oui
        //File file4 = new File("files/Modele.Edt.json");
        Edt edt1 = readJsonEdt(file2);**/

    }
}
