package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Job {
    private final int id;
    private Location loc;
    private List<Abilities> abL;
    private TimeInterval horairenec;
    private TimeInterval horairevoulu;
    private long duree; //duree = un nombre en secondes

@JsonCreator
    public Job(@JsonProperty("id") int id,@JsonProperty("loc") Location loc,@JsonProperty("abL") List<Abilities> abL,@JsonProperty("hv") TimeInterval hv,@JsonProperty("hn") TimeInterval hn, @JsonProperty("dur") long dur)
    {
        this.id = id;
        this.loc = loc;
        this.abL =abL;
        this.horairevoulu = hv;
        this.horairenec = hn;
        this.duree = dur;
    }


    public Job(int id,Location loc,List<Abilities> abL, TimeInterval hn, long dur)
    {
        this.id = id;
        this.loc = loc;
        this.abL =abL;
        this.horairenec = hn;
        this.horairevoulu = hn;
        this.duree = dur;
    }

    public TimeInterval getHorairenec() {
        return horairenec;
    }

    public long getDuree() {
        return duree;
    }

    public void setHorairenec(TimeInterval horairenec) {
        this.horairenec = horairenec;
    }

    public TimeInterval getHorairevoulu() {
        return horairevoulu;
    }

    public void setHorairevoulu(TimeInterval horairevoulu) {
        this.horairevoulu = horairevoulu;
    }

    public void setDuree(long duree) { this.duree = duree; }


    public int getId() {
        return id;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public List<Abilities> getAbL() {
        return abL;
    }

    public void setAbL(List<Abilities> abL) {
        this.abL = abL;
    }

    public void addAbL(Abilities a) {
        this.abL.add(a);
    }

    public void removeAbL(Abilities a) {
        this.abL.remove(a);
    }

    public String toString()
    {
        String competences = "";
        for (int i = 0; i < this.abL.size(); i++) {
             competences += this.abL.get(i) +" : ";
        }
        return "Modele.Job "+this.id+" : "+this.loc+" "+this.horairenec +" "+ competences;
    }

}
