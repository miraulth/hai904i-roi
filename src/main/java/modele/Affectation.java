package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Affectation {

    private TimeInterval horaire;
    private Job travail;
@JsonCreator
    public Affectation(@JsonProperty("horaire")TimeInterval horaire,@JsonProperty("travail") Job travail) {
        this.horaire = horaire;
        this.travail = travail;
    }

    public TimeInterval getHoraire() {
        return horaire;
    }

    public void setHoraire(TimeInterval horaire) {
        this.horaire = horaire;
    }

    public Job getTravail() {
        return travail;
    }

    public void setTravail(Job travail) {
        this.travail = travail;
    }
}
