package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Edt {

    private Employee employe;
    private List<TimeInterval> timeTravel;
    private List<Affectation> affectation;
@JsonCreator
    public Edt(@JsonProperty("employe") Employee employe,@JsonProperty("affectation") List<Affectation> affectation,@JsonProperty("timeTravel") List<TimeInterval> timeTravel) {
        this.employe = employe;
        this.affectation = affectation;
        this.timeTravel = timeTravel;
    }

    public Edt(Employee employe,Affectation affectation,TimeInterval timeTravel) {
        this.employe = employe;
        this.affectation = new ArrayList<>();
        this.affectation.add(affectation);
        this.timeTravel = new ArrayList<>();
        this.timeTravel.add(timeTravel);
    }

    public Edt(Employee employe) {
        this.employe = employe;
        this.affectation = new ArrayList<>();
        this.timeTravel = new ArrayList<>();
    }

    public Employee getEmploye() {
        return employe;
    }

    public void setEmploye(Employee employe) {
        this.employe = employe;
    }

    public List<Affectation> getAffectation() {
        return affectation;
    }

    public void setAffectation(List<Affectation> affectation) {
        this.affectation = affectation;
    }

    public List<TimeInterval> getTimeTravel() {
        return timeTravel;
    }

    public void setTimeTravel(List<TimeInterval> timeTravel) {
        this.timeTravel = timeTravel;
    }

    public void removeAll()
    {
        this.setAffectation(new ArrayList<>());
        this.setTimeTravel(new ArrayList<>());
    }

    public String toString() {
        System.out.println(this.employe.getHoraires());
        String affectationslist ="Les jobs attitrés sont les : ";// ""+this.employe.getHoraires().get(0).getBeg().truncatedTo(ChronoUnit.SECONDS)+"\n";
        for (int i = 0; i < this.affectation.size(); i++) {
            affectationslist += "Tps de trajet : " + this.getTimeTravel().get(i).time()+"s  ====>  ";
            affectationslist += "[" + this.affectation.get(i).getHoraire().getBeg() + "  ||  " + this.affectation.get(i).getHoraire().getEnd() + "]";
            affectationslist += "Job "+affectation.get(i).getTravail().getId()+"\n";
        }
        if(affectation.isEmpty()) affectationslist+=employe.getHoraires();
        return "Edt : Employee = " + employe.getId() + "\n" +
                affectationslist +
                "==============";
    }

    public boolean affectationIsAfter(int timeFromLastJob,int posAffectation,int posHoraires){
        boolean possible = true;
        boolean existPossibleInterval = false;
        if(this.affectation.get(posAffectation).getHoraire().getBeg().minusSeconds(timeFromLastJob).isBefore(this.getEmploye().getHoraires().get(posHoraires).getBeg())){
            if(posHoraires==0){
                possible = false;
            }else{
                int k = posHoraires-1;
                while((k>=0)&&(possible)&&!(existPossibleInterval)){
                    if(this.getEmploye().getHoraires().get(k).getBeg().isAfter(this.affectation.get(posAffectation-1).getHoraire().getEnd())){
                        existPossibleInterval = (this.getEmploye().getHoraires().get(k).getEnd().minusSeconds(timeFromLastJob).isAfter(this.getEmploye().getHoraires().get(k).getBeg()));
                    }else if (this.getEmploye().getHoraires().get(k).getEnd().minusSeconds(timeFromLastJob).isBefore(this.affectation.get(posAffectation-1).getHoraire().getEnd())){
                        possible = false;
                    }
                    k--;
                }
            }
        }
        if(this.affectation.get(posAffectation).getHoraire().getBeg().isBefore(this.getEmploye().getHoraires().get(posHoraires).getBeg())) {
            possible = false;
        }
        return possible;
    }
    public boolean affectationIsBefore(int timeToNextJob,int posAffectation,int posHoraires) {
        boolean existPossibleInterval= false;
        boolean possible = true;
        if(this.affectation.get(posAffectation).getHoraire().getEnd().plusSeconds(timeToNextJob).isAfter(this.getEmploye().getHoraires().get(posHoraires).getEnd())){
            if(posHoraires == this.getEmploye().getHoraires().size()-1){
                possible = false;
            }else{
                int k = posHoraires + 1;
                while((k<this.getEmploye().getHoraires().size())&&(possible)&&(!existPossibleInterval)){
                    if(this.getEmploye().getHoraires().get(k).getEnd().isBefore(this.affectation.get(posAffectation+1).getHoraire().getBeg())){
                        existPossibleInterval = (this.getEmploye().getHoraires().get(k).getEnd().minusSeconds(timeToNextJob).isAfter(this.getEmploye().getHoraires().get(k).getBeg()));
                    }else {
                        existPossibleInterval = this.getEmploye().getHoraires().get(k).getBeg().plusSeconds(timeToNextJob).isBefore(this.affectation.get(posAffectation+1).getHoraire().getEnd());
                        possible = existPossibleInterval;
                    }
                    k++;
                }
            }
        }
        if(this.affectation.get(posAffectation).getHoraire().getEnd().isAfter(this.getEmploye().getHoraires().get(posHoraires).getEnd())) {
            possible = false;
        }
        return possible;
    }


        public boolean affectationIsClear(int timeFromLastJob,int timeToNextJob,int posAffectation,int posHoraires)
    {
        boolean possible = false;

        if(posAffectation==0){
            possible = firstAffectationIsRight(timeFromLastJob, posAffectation, posHoraires);
        }else{
            possible = affectationIsAfter(timeFromLastJob, posAffectation, posHoraires);
        }
        if (posAffectation==this.affectation.size()-1) {
            possible = possible && lastAffectationIsRight(timeToNextJob, posAffectation, posHoraires);
        }else {
            possible = possible && affectationIsBefore(timeToNextJob, posAffectation, posHoraires);
        }
        return possible;
    }

    private boolean firstAffectationIsRight(int timeFromLastJob, int posAffectation, int posHoraires) {
        boolean existPossibleInterval;
        boolean possible = true;
        if(this.affectation.get(posAffectation).getHoraire().getBeg().minusSeconds(timeFromLastJob).isBefore(this.getEmploye().getHoraires().get(posHoraires).getBeg())){
            if(posHoraires == 0){
                possible = false;
            }else{
                existPossibleInterval = false;
                int k = posHoraires - 1;
                while((k>=0)&&(!existPossibleInterval)){
                    existPossibleInterval = this.getEmploye().getHoraires().get(k).getEnd().minusSeconds(timeFromLastJob).isAfter(this.getEmploye().getHoraires().get(k).getBeg());
                    k--;
                }
                if(!existPossibleInterval){
                    possible = false;
                }
            }
        }
        return possible;
    }

    private boolean lastAffectationIsRight(int timeToNextJob, int posAffectation, int posHoraires) {
        boolean existPossibleInterval;
        boolean possible = true;
        if(this.affectation.get(posAffectation).getHoraire().getEnd().plusSeconds(timeToNextJob).isAfter(this.getEmploye().getHoraires().get(posHoraires).getEnd())){
            if(posHoraires == this.getEmploye().getHoraires().size()-1){
                possible = false;
            }else{
                existPossibleInterval = false;
                int k = posHoraires + 1;
                while((k<this.getEmploye().getHoraires().size())&&(!existPossibleInterval)){
                    existPossibleInterval = this.getEmploye().getHoraires().get(k).getBeg().plusSeconds(timeToNextJob).isBefore(this.getEmploye().getHoraires().get(k).getEnd());
                    k++;
                }
                possible = existPossibleInterval;
            }
        }
        return possible;
    }

    public boolean edtIsClear()
    {
        boolean possible = true;
        int i = 0;
        if(this.affectation.size() == this.timeTravel.size()-1){
            while((i < this.affectation.size()) && (possible))
            {
                int j=0;
                while((j<this.getEmploye().getHoraires().size())&&(this.getEmploye().getHoraires().get(j).getEnd().isBefore(this.affectation.get(i).getHoraire().getBeg()))){
                    j++;
                }
                if (j != this.getEmploye().getHoraires().size()){
                    int timeFromLastJob = this.timeTravel.get(i).time();
                    int timeToNextJob = this.timeTravel.get(i+1).time();
                    possible = affectationIsClear(timeFromLastJob,timeToNextJob,i,j);

                }else {
                    possible = false;
                }
                i = i + 1;
            }
        } else if(!this.affectation.isEmpty()){
            possible = false;
        }
        if (!possible) System.out.println("La "+i+" affectation pose problème");
        return possible;
    }

    public int addAffectation(Affectation a)
    {
        int i = 0;
        //on peut faire mieux avec une recherche dichotomique
        while((i < this.affectation.size()) && (this.affectation.get(i).getHoraire().getBeg().isBefore(a.getHoraire().getBeg())))
        {
            i = i + 1;
        }
        if (this.getAffectation().isEmpty())
        {
            int timeFromHome = a.getTravail().getLoc().timeToGoTo(this.employe.getLocbeg(),60);
            int timeToHome = a.getTravail().getLoc().timeToGoTo(this.employe.getLocend(),60);
            TimeInterval tI1 = new TimeInterval(timeFromHome,a.getHoraire().getBeg());
            TimeInterval tI2 = new TimeInterval(a.getHoraire().getEnd(),timeToHome);
            this.timeTravel.add(tI1);
            this.timeTravel.add(tI2);
            this.getAffectation().add(a);
        } else if (i == this.getAffectation().size())
        {
            int timeToHome = a.getTravail().getLoc().timeToGoTo(this.employe.getLocend(),60);
            int timeToComeLastJob = a.getTravail().getLoc().timeToGoTo(this.getAffectation().get(i-1).getTravail().getLoc(),60);
            TimeInterval tI1 = new TimeInterval(timeToComeLastJob,a.getHoraire().getBeg());
            TimeInterval tI2 = new TimeInterval(a.getHoraire().getEnd(),timeToHome);
            this.timeTravel.remove(i);
            this.timeTravel.add(tI1);
            this.timeTravel.add(tI2);
            this.getAffectation().add(a);
        } else if (i == 0)
        {
            int timeFromHome = a.getTravail().getLoc().timeToGoTo(this.employe.getLocbeg(),60);
            int timeToSecondJob = a.getTravail().getLoc().timeToGoTo(this.getAffectation().get(i).getTravail().getLoc(),60);
            TimeInterval tI1 = new TimeInterval(timeFromHome,a.getHoraire().getBeg());
            TimeInterval tI2 = new TimeInterval(a.getHoraire().getEnd(),timeToSecondJob);
            this.timeTravel.remove(i);
            this.timeTravel.add(0,tI1);
            this.timeTravel.add(1,tI2);
            this.getAffectation().add(i,a);
        } else
        {
            int timeToGoOtherJob = a.getTravail().getLoc().timeToGoTo(this.getAffectation().get(i).getTravail().getLoc(),60);
            int timeToComeLastJob = a.getTravail().getLoc().timeToGoTo(this.getAffectation().get(i-1).getTravail().getLoc(),60);
            TimeInterval tI1 = new TimeInterval(timeToComeLastJob,a.getHoraire().getBeg());
            TimeInterval tI2 = new TimeInterval(a.getHoraire().getEnd(),timeToGoOtherJob);
            this.timeTravel.remove(i);
            this.timeTravel.add(i,tI1);
            this.timeTravel.add(i+1,tI2);
            this.getAffectation().add(i,a);
        }
        return i;
    }
}
