package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Employee {
    private final int id;
    private Location locbeg;
    private Location locend;
    private List<Abilities> abL;
    private List<TimeInterval> horaires;


@JsonCreator
    public Employee(@JsonProperty("id") int id,@JsonProperty("lb") Location lb,@JsonProperty("le") Location le,@JsonProperty("abL") List<Abilities> abL,@JsonProperty("h") List<TimeInterval> h)
    {
        this.id = id;
        this.locbeg = lb;
        this.locend = le;
        this.abL = abL;
        this.horaires = h;
    }

    public int getId() {
        return id;
    }

    public Location getLocbeg() {
        return locbeg;
    }

    public void setLocbeg(Location locbeg) {
        this.locbeg = locbeg;
    }

    public Location getLocend() {
        return locend;
    }

    public void setLocend(Location locend) {
        this.locend = locend;
    }

    public List<Abilities> getAbL() {
        return abL;
    }

    public void setAbL(List<Abilities> abL) {
        this.abL = abL;
    }

    public List<TimeInterval> getHoraires() {
        return horaires;
    }

    public void setHoraires(List<TimeInterval> horaires) {
        this.horaires = horaires;
    }

    public String toString()
    {
        return "Employe "+this.id+" : "+this.locbeg+" "+this.locend+" "+getAbL()+" "+getHoraires();
    }
}