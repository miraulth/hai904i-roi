package modele;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {
    private final int id;
    private float coordx;
    private float coordy;
@JsonCreator
    public Location(@JsonProperty("id")int id,@JsonProperty("coordx") float coordx,@JsonProperty("coordy") float coordy) {
        this.id = id;
        this.coordx = coordx;
        this.coordy = coordy;
    }

    public float getCoordx() {
        return coordx;
    }

    public void setCoordx(float coordx) {
        this.coordx = coordx;
    }

    public float getCoordy() {
        return coordy;
    }

    public void setCoordy(float coordy) {
        this.coordy = coordy;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return " x = " + this.coordx + " y = " + this.coordy;
    }


    /*
     * Point NO :43.563972, 4.085444 = La Grande Motte
     *            N/S          E/O
     *   1latitude = 111km   1longitude = 80km (en France)
     * Point SE :43.912559, 3.512650  = Blandas
     * */
    public int timeToGoTo(Location l,int averageSpeed){
        float kmxl1 = this.getCoordx()*111;
        float kmyl1 = this.getCoordy()*80;
        float kmxl2 = l.getCoordx()*111;
        float kmyl2 = l.getCoordy()*80;
        double dist = Math.sqrt(Math.pow((kmxl1-kmxl2),2)+Math.pow((kmyl2-kmyl1),2));
        double temps = (dist/averageSpeed);
        return (int)(temps*3600);//Renvoie le temps en Secondes
    }
    public int distanceWith(Location l){
        float kmxl1 = this.getCoordx()*111;
        float kmyl1 = this.getCoordy()*80;
        float kmxl2 = l.getCoordx()*111;
        float kmyl2 = l.getCoordy()*80;
        double dist = Math.sqrt(Math.pow((kmxl1-kmxl2),2)+Math.pow((kmyl2-kmyl1),2));
        return (int)(dist*1000);
    }

}
