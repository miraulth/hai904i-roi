package resolution;

import modele.Main;
import modele.WsrModel;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class Solver{

    public static void main(String[] args){
        File f = new File("files/Instance_10.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
        }
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();

        Instant beg = Instant.now().plusSeconds(999999999);
        Instant end = Instant.now().minusSeconds(999999999);
        for(int i=0;i<model.getJobs().size();i++){
            if(model.getJobs().get(i).getHorairenec().getBeg().getEpochSecond() < beg.getEpochSecond()){
                beg = model.getJobs().get(i).getHorairenec().getBeg();
            }
            if(model.getJobs().get(i).getHorairenec().getEnd().getEpochSecond() > end.getEpochSecond()){
                end = model.getJobs().get(i).getHorairenec().getEnd();
            }
        }
        for(int i=0;i<model.getLemploye().size();i++){
            if(model.getLemploye().get(i).getHoraires().get(0).getBeg().getEpochSecond() < beg.getEpochSecond()){
                beg = model.getJobs().get(i).getHorairenec().getBeg();
            }
            if(model.getLemploye().get(i).getHoraires().get(0).getEnd().getEpochSecond() > end.getEpochSecond()){
                end = model.getLemploye().get(i).getHoraires().get(0).getEnd();
            }
        }
        MIP solverMIP = new MIP(model, beg, end,10000,60,999999,1.0f,0.1f,0.0001f,0.0001f);

        Duration d = Duration.ofSeconds(30);
        solverMIP.solveMIP(d);

    }
}
