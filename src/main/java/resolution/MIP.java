package resolution;

import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;
import com.skaggsm.ortools.OrToolsHelper;
import modele.*;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MIP{
    private List<Edt> solution;
    private WsrModel model;
    private Instant begEdt;
    private Instant endEdt;
    private List<Employee> K;
    private List<Job> I;
    private List<Location> theta;
    private List<Location> I2;  // I'
    private List<List<Integer>> L;
    private List<List<TimeInterval>> F;
    private List<Integer> alpha;
    private List<Instant> a;
    private List<Instant> b;
    private List<Instant> a2; //ak
    private List<Instant> b2; //bk
    private List<List<Instant>> a3; //al
    private List<Long> sigma;
    private List<List<Long>> sigma2;  //sigma lk
    private List<List<Integer>> d;
    private List<List<Integer>> t;
    private List<List<Integer>> s;
    private int maxNbBreak;
    private int M;
    private float beta;
    private float gamma;
    private float psi;
    private float phi;

    public List<Edt> getSolution() {
        return solution;
    }
    public WsrModel getModel() {
        return model;
    }

    public MIP(WsrModel model,Instant beg,Instant end,int vAlpha,int vSpeed,int vM,float vBeta,float vGamma,float vPsi,float vPhi) {
        this.begEdt = beg;
        this.endEdt = end;
        this.K = model.getLemploye();
        this.I = model.getJobs();
        this.theta = new ArrayList<>();
        for(int i=0;i<K.size();i++){
            this.theta.add(K.get(i).getLocbeg());
        }
        this.I2 = new ArrayList<>();
        for(int i=0;i<model.getJobs().size();i++){
            this.I2.add(model.getJobs().get(i).getLoc());
        }
        for(int i=0;i<theta.size();i++){
            this.I2.add(theta.get(i));
        }
        this.L = new ArrayList<>();
        for(int i=0;i<K.size();i++){
            this.L.add(new ArrayList<>());
            for(int j=0;j<K.get(i).getHoraires().size()-1;j++){
                this.L.get(i).add(j);
            }
        }
        for(int i=0;i<K.size();i++){
            if(this.maxNbBreak < L.get(i).size()){
                this.maxNbBreak = L.get(i).size();
            }
        }
        this.F = new ArrayList<>();
        for(int i=0;i<I.size();i++){
            this.F.add(new ArrayList<>());
            this.F.get(i).add(I.get(i).getHorairenec());
            this.F.get(i).add(I.get(i).getHorairevoulu());
        }
        this.alpha = new ArrayList<>();
        for(int i=0;i<I.size();i++){
            this.alpha.add(vAlpha);
        }
        this.a = new ArrayList<>();
        for(int i=0;i<F.size();i++){
            this.a.add(this.F.get(i).get(0).getBeg());
        }
        this.b = new ArrayList<>();
        for(int i=0;i<F.size();i++){
            this.b.add(this.F.get(i).get(0).getEnd().minusSeconds(this.I.get(i).getDuree()));
        }
        this.a2 = new ArrayList<>();
            for (int i=0;i<K.size();i++){
                this.a2.add(K.get(i).getHoraires().get(0).getBeg());
            }
        this.b2 = new ArrayList<>();
        for (int i=0;i<K.size();i++){
            this.b2.add(K.get(i).getHoraires().get(K.get(i).getHoraires().size()-1).getEnd());
        }
        this.a3 = new ArrayList<>();
        for (int i=0;i<K.size();i++){
            this.a3.add(new ArrayList<>());
            for (int j=0;j<L.get(i).size();j++){
                this.a3.get(i).add(K.get(i).getHoraires().get(j).getEnd());
            }
        }
        this.sigma = new ArrayList<>();
        for(int i=0;i<I.size();i++){
            this.sigma.add(I.get(i).getDuree());
        }
        this.sigma2 = new ArrayList<>();
        for (int i=0;i<K.size();i++){
            this.sigma2.add(new ArrayList<>());
            for (int j=0;j<L.get(i).size();j++){
                this.sigma2.get(i).add(K.get(i).getHoraires().get(j+1).getBeg().getEpochSecond()-K.get(i).getHoraires().get(j).getEnd().getEpochSecond());
            }
        }
        this.d = new ArrayList<>();
        for(int i=0;i<I2.size();i++){
            this.d.add(new ArrayList<>());
            for(int j=0;j<I2.size();j++){
                this.d.get(i).add(I2.get(i).distanceWith(I2.get(j)));
            }
        }
        this.t = new ArrayList<>();
        for(int i=0;i<I2.size();i++){
            this.t.add(new ArrayList<>());
            for(int j=0;j<I2.size();j++){
                this.t.get(i).add(I2.get(i).timeToGoTo(I2.get(j),vSpeed));
            }
        }
        this.s = new ArrayList<>();
        for(int i=0;i<K.size();i++){
            this.s.add(new ArrayList<>());
            for(int j=0;j<I.size();j++){
                if(K.get(i).getAbL().get(0).compatibleSkills(I.get(j).getAbL(),K.get(i).getAbL())){
                    this.s.get(i).add(1);
                }else{
                    this.s.get(i).add(0);
                }

            }
        }
        this.M = vM;
        this.beta = vBeta;
        this.gamma = vGamma;
        this.psi = vPsi;
        this.phi = vPhi;

        this.solution = new ArrayList<>();
        for (int i = 0; i < model.getLemploye().size(); i++) {
            solution.add(new Edt(model.getLemploye().get(i)));
        }
        this.model = model;
    }
    static {
        // Somewhere before using OR-Tools classes
        OrToolsHelper.loadLibrary();
    }
    public boolean solveMIP(Duration timeLimit) {
        boolean  isSolve = true;
        MPSolver solver = new MPSolver(
                "SimpleMipProgram", MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING);
        double infinity = Double.POSITIVE_INFINITY;
        solver.setTimeLimit(timeLimit.getSeconds()*1000);

        MPVariable[] fv = solver.makeIntVarArray(I.size(),0,1);
        MPVariable[] f = solver.makeIntVarArray(I.size(),0,1);
        MPVariable[] u = solver.makeIntVarArray(K.size(),0,1);
        MPVariable[][] y = new MPVariable[I.size()][K.size()];
        for (int i = 0; i < I.size(); i++) {
            for (int j = 0; j < K.size(); j++) {
                y[i][j]= solver.makeIntVar(0,1,"");
            }
        }
        MPVariable[][][] x = new MPVariable[K.size()][I2.size()][I2.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I2.size(); j++) {
                for (int k = 0; k < I2.size(); k++) {
                    x[i][j][k]= solver.makeIntVar(0,1,"");
                }
            }
        }
        MPVariable[][][][] W = new MPVariable[K.size()][I.size()][K.size()][maxNbBreak];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                for (int k = 0; k < L.size(); k++) {
                    for (int l = 0; l < L.get(k).size(); l++) {
                        W[i][j][k][l]= solver.makeIntVar(0,1,"");
                    }
                }
            }
        }
        MPVariable[][] nW = new MPVariable[K.size()][maxNbBreak];
        for (int i = 0; i < K.size(); i++) {
            for (int k = 0; k < L.get(i).size(); k++) {
                nW[i][k] = solver.makeIntVar(0,1,"");
            }

        }
        MPVariable[] T = solver.makeIntVarArray(I.size(),0,timeInSecondFromBeg(endEdt));
        MPVariable[][] Tl = new MPVariable[K.size()][maxNbBreak];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < L.get(i).size(); j++) {
                Tl[i][j]= solver.makeIntVar(0,timeInSecondFromBeg(endEdt),"");
            }
        }
        System.out.println("Number of variables = {"+solver.numVariables()+"}");

        MPConstraint[] c0 = new MPConstraint[I.size()];
        for (int i = 0; i < I.size(); i++) {
            c0[i] = solver.makeConstraint(0,1);
            for (int j = 0; j < K.size(); j++) {
                c0[i].setCoefficient(y[i][j],1);
            }
        }
        MPConstraint[][] c1 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c1[i][j] = solver.makeConstraint(0,s.get(i).get(j));
                c1[i][j].setCoefficient(y[j][i],1);
            }
        }
        MPConstraint[] c2 = new MPConstraint[K.size()];
        for (int i = 0; i < K.size(); i++) {
            c2[i] = solver.makeConstraint(1,1);
            c2[i].setCoefficient(u[i],1);
            for (int j = 0; j < I.size(); j++) {
                c2[i].setCoefficient(x[i][I.size() + i][j], 1);
            }
        }
        MPConstraint[][] c3 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c3[i][j] = solver.makeConstraint(0,0);
                c3[i][j].setCoefficient(y[j][i],-1);
                c3[i][j].setCoefficient(x[i][I.size() + i][j], 1);
                for (int k = 0; k < I.size(); k++) {
                    c3[i][j].setCoefficient(x[i][k][j], 1);
                }
            }
        }
        MPConstraint[] c4 = new MPConstraint[K.size()];
        for (int i = 0; i < K.size(); i++) {
            c4[i] = solver.makeConstraint(0,0);
            for (int j = 0; j < I.size(); j++) {
                c4[i].setCoefficient(x[i][I.size() + i][j], 1);
                c4[i].setCoefficient(x[i][j][I.size() + i], -1);
            }
        }
        MPConstraint[][] c5 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c5[i][j] = solver.makeConstraint(0,0);
                c5[i][j].setCoefficient(x[i][I.size() + i][j], 1);
                c5[i][j].setCoefficient(x[i][j][I.size() + i], -1);
                for (int k = 0; k < I.size(); k++) {
                    c5[i][j].setCoefficient(x[i][k][j], 1);
                    c5[i][j].setCoefficient(x[i][j][k], -1);
                }
            }
        }
        MPConstraint[][][] c6 = new MPConstraint[K.size()][I.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                for (int k = 0; k < I.size(); k++) {
                    c6[i][j][k] = solver.makeConstraint(-infinity,M);
                    c6[i][j][k].setCoefficient(T[j], 1);
                    c6[i][j][k].setCoefficient(T[k], -1);
                    c6[i][j][k].setCoefficient(x[i][j][k], M+t.get(j).get(k)+sigma.get(j));
                    for (int l = 0; l < L.get(i).size(); l++) {
                        c6[i][j][k].setCoefficient(W[i][j][i][l], sigma2.get(i).get(l));
                    }
                }
            }
        }

        MPConstraint[][] c7 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c7[i][j] = solver.makeConstraint(-infinity,M-timeInSecondFromBeg(a2.get(i)));
                c7[i][j].setCoefficient(T[j],-1);
                c7[i][j].setCoefficient(x[i][I.size() + i][j], t.get(I.size() + i).get(j)+M);
            }
        }
        MPConstraint[][] c8 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c8[i][j] = solver.makeConstraint(-infinity,M-sigma.get(j)+timeInSecondFromBeg(b2.get(i))-t.get(j).get(I.size() + i));
                c8[i][j].setCoefficient(T[j], 1);
                c8[i][j].setCoefficient(x[i][j][I.size() + i], M);
                for (int l = 0; l < L.get(i).size(); l++) {
                    c8[i][j].setCoefficient(W[i][j][i][l], sigma2.get(i).get(l));
                }
            }
        }

        MPConstraint[] c9 = new MPConstraint[I.size()];
        for(int i=0;i<I.size();i++){
            c9[i] = solver.makeConstraint(-infinity,0);
            c9[i].setCoefficient(T[i], -1);
            c9[i].setCoefficient(f[i], timeInSecondFromBeg(a.get(i)));
        }
        MPConstraint[] c10 = new MPConstraint[I.size()];
        for(int i=0;i<I.size();i++){
            c10[i] = solver.makeConstraint(-infinity,0);
            c10[i].setCoefficient(T[i], -1);
            c10[i].setCoefficient(f[i], timeInSecondFromBeg(b.get(i)));
        }
        MPConstraint[] c11 = new MPConstraint[I.size()];
        for(int i=0;i<I.size();i++){
            c11[i] = solver.makeConstraint(0,0);
            c11[i].setCoefficient(f[i], -1);
            for(int j=0;j<K.size();j++){
                c11[i].setCoefficient(y[i][j], 1);
            }
        }
        MPConstraint[] c12 = new MPConstraint[I.size()];
        for (int i = 0; i < I.size(); i++) {
            c12[i] = solver.makeConstraint(-infinity,0);
            c12[i].setCoefficient(f[i], -1);
            c12[i].setCoefficient(fv[i], 1);
        }
        MPConstraint[] c13 = new MPConstraint[K.size()];
        for (int i = 0; i < K.size(); i++) {
            c13[i] = solver.makeConstraint(1,1);
            c13[i].setCoefficient(u[i], 1);
            for (int l = 0; l < L.get(i).size(); l++) {
                c13[i].setCoefficient(nW[i][l], 1);
                for (int j = 0; j < I.size(); j++) {
                    c13[i].setCoefficient(W[i][j][i][l], 1);
                }
            }
        }
        MPConstraint[][] c14 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c14[i][j] = solver.makeConstraint(0,1);
                c14[i][j].setCoefficient(y[j][i], 1);
                for (int k = 0; k < L.get(i).size(); k++) {
                    c14[i][j].setCoefficient(W[i][j][i][k], -1);
                }
            }
        }
        MPConstraint[][] c15 = new MPConstraint[K.size()][I.size()];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < I.size(); j++) {
                c15[i][j] = solver.makeConstraint(-infinity,M-sigma.get(j));
                c15[i][j].setCoefficient(T[j], 1);
                for (int k = 0; k < L.get(i).size(); k++) {
                    c15[i][j].setCoefficient(W[i][j][i][k], M);
                    c15[i][j].setCoefficient(Tl[i][k], -1);
                }
            }
        }
        MPConstraint[][] c16 = new MPConstraint[K.size()][maxNbBreak];
        for (int i = 0; i < K.size(); i++) {
            for (int j = 0; j < L.get(i).size(); j++) {
                c16[i][j] = solver.makeConstraint(timeInSecondFromBeg(a3.get(i).get(j)),timeInSecondFromBeg(a3.get(i).get(j)));
                c16[i][j].setCoefficient(Tl[i][j], 1);
                c16[i][j].setCoefficient(nW[i][j], timeInSecondFromBeg(a3.get(i).get(j)));
                c16[i][j].setCoefficient(u[i], timeInSecondFromBeg(a3.get(i).get(j)));
            }
        }

        System.out.println("Number of constraints = {"+solver.numConstraints()+"}");

        MPObjective objective = solver.objective();
        for (int i = 0; i < I.size(); i++) {
            for (int j=0;j < K.size();j++){
                objective.setCoefficient(y[i][j], alpha.get(i)*beta);
            }
        }
        for (int i = 0; i < K.size(); i++) {
            for (int j=0;j < I2.size();j++){
                for (int k=0;k < I2.size();k++){
                    objective.setCoefficient(x[i][j][k], -(d.get(i).get(j)*gamma));
                }
            }
        }
        for (int i = 0; i < K.size(); i++) {
            for (int j=0;j < L.get(i).size();j++){
                objective.setCoefficient(nW[i][j], -phi);
            }
        }
        objective.setMaximization();

        System.out.println("Solving...");
        final MPSolver.ResultStatus resultStatus = solver.solve();
        System.out.println("Résultat" +resultStatus);
        if(resultStatus == null){
            isSolve = false;
        }else {
            System.out.println("Création de l'edt correspondant à la solution...");
            for(int i=0;i<I.size();i++){
                for(int j=0;j<K.size();j++){
                    if(y[i][j].solutionValue() > 0){
                        Instant instant = begEdt.plusSeconds((int)T[i].solutionValue());
                        TimeInterval tI = new TimeInterval(instant,sigma.get(i).intValue());
                        Affectation a = new Affectation(tI,I.get(i));
                        solution.get(j).addAffectation(a);
                    }
                }
            }
        }
        //assertEquals(MPSolver.ResultStatus.OPTIMAL, resultStatus);

        if (resultStatus == MPSolver.ResultStatus.OPTIMAL) {
            System.out.println("Solution:");
            System.out.println("Objective value = {"+objective.value()+"}");

            System.out.println("\nAdvanced usage:");
            System.out.println("Problem solved in {"+solver.wallTime()+"} milliseconds");
            System.out.println("Problem solved in {"+solver.iterations()+"} iterations");
            System.out.println("Problem solved in {"+solver.nodes()+"} branch-and-bound nodes");
        } else {
            System.out.println("La solution optimale n'a pas était trouvée!");
        }


        return isSolve;
    }
    public int timeInSecondFromBeg(Instant i){
        return (int)(i.getEpochSecond()-begEdt.getEpochSecond());
    }
    public int timeInMinuteFromBeg(Instant i){
        return (int)((i.getEpochSecond()-begEdt.getEpochSecond())/60);
    }

}
