package resolution;


import modele.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Metah {
    private WsrModel modele;
    private List<Edt> solution;

    public WsrModel getModele() {
        return modele;
    }

    public List<Edt> getSolution() {
        return solution;
    }

    public Location getLocOfTheLastAffectation(List<Affectation> affectationList, Employee e){
        if (affectationList.isEmpty()){
            return e.getLocbeg();
        }
        else
            return affectationList.get(affectationList.size()-1).getTravail().getLoc();
    }

    public boolean affectToEmploye_horairevoulu(Job j, Employee e,int indiceEmp) {
        //On va regarder à chaque fois si entre la tâche actuelle et la suivante on peut insérer le job j
        Location lastLocation = e.getLocbeg();
        TimeInterval horaireJob = j.getHorairevoulu();
        Location jobLocation = j.getLoc();
        List<Affectation> cur_edt = solution.get(indiceEmp).getAffectation();

        List<TimeInterval> horaireEmploye = e.getHoraires();
        int curAffect = 0;
        for(int i = 0; i <  horaireEmploye.size(); i++){
            //On raisonne par créneaux possibles où insérer le job
            Instant horaire_debut_creneau = horaireEmploye.get(i).getBeg();
            Instant horaire_fin_creneau = horaireEmploye.get(i).getEnd();
            Instant debut_creneau_possible;
            Instant fin_creneau_possible;
            Boolean rentre_creneau = false; //va tester si le job rentre dans le créneau
            Boolean ok_next_job = false; //va tester si le job est cohérent avec le trajet pour le job suivant
            if(horaire_debut_creneau.isAfter(horaireJob.getBeg()))
                debut_creneau_possible = horaire_debut_creneau;
            else
                debut_creneau_possible = horaireJob.getBeg();
            if(horaire_fin_creneau.isBefore(horaireJob.getEnd()))
                fin_creneau_possible = horaire_fin_creneau;
            else
                fin_creneau_possible = horaireJob.getEnd();
            if(!(horaire_debut_creneau.isAfter(horaireJob.getEnd()) || horaire_fin_creneau.isBefore(horaireJob.getBeg()))){
                    while (curAffect < cur_edt.size() && cur_edt.get(curAffect).getHoraire().getBeg().isBefore(horaire_fin_creneau)) {
                        if(horaire_fin_creneau.isBefore(horaireJob.getEnd()))
                            fin_creneau_possible = horaire_fin_creneau;
                        else
                            fin_creneau_possible = horaireJob.getEnd();
                        fin_creneau_possible = cur_edt.get(curAffect).getHoraire().getBeg();
                        if (!(fin_creneau_possible.isBefore(debut_creneau_possible))) {
                            //on teste si le job rentre dans le créneau (donc déplacement du dernier lieu + temps du job < fin du créneau)
                            rentre_creneau = (debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree())).isBefore(fin_creneau_possible);
                            Location nextLocation;
                            //maintenant on vérifie la cohérence pour le job suivant
                            if (curAffect == cur_edt.size() - 1) {
                                //si c'était le dernier job de l'EDT : l'employé doit rentrer chez lui
                                nextLocation = e.getLocbeg();
                                int curHoraireEmploye = i;
                                Instant curtime = debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree());
                                //curtime va être égal au temps où on en est; la fin du job qu'on vient d'insérer pour l'instant puis le début du créneau horaire
                                //suivant de l'employé
                                while (curHoraireEmploye < horaireEmploye.size()) {
                                    ok_next_job = ok_next_job || (curtime.plusSeconds(jobLocation.timeToGoTo(nextLocation, 60)).isBefore(horaireEmploye.get(curHoraireEmploye).getEnd()));
                                    curHoraireEmploye++;
                                    if (curHoraireEmploye < horaireEmploye.size())
                                        curtime = horaireEmploye.get(curHoraireEmploye).getBeg();
                                }
                            } else {
                                //sinon : il y a une autre affectation après
                                nextLocation = cur_edt.get(curAffect + 1).getTravail().getLoc();
                                int curHoraireEmploye = i;
                                //même idée que précédemment
                                Instant curtime = debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree());
                                while (curHoraireEmploye < horaireEmploye.size() && cur_edt.get(curAffect + 1).getHoraire().getBeg().isAfter(horaireEmploye.get(curHoraireEmploye).getEnd())) {
                                    curHoraireEmploye++;
                                    curtime = horaireEmploye.get(curHoraireEmploye).getBeg();
                                }
                                ok_next_job = curtime.plusSeconds(jobLocation.timeToGoTo(nextLocation, 60)).isBefore(cur_edt.get(curAffect + 1).getHoraire().getBeg());
                            }
                        }
                        if (rentre_creneau && ok_next_job) {
                            //gagné
                            //il faut insérer le job
                            TimeInterval jobdone = new TimeInterval(debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)), debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree()));
                            Affectation temp = new Affectation(jobdone, j);
                            solution.get(indiceEmp).addAffectation(temp);
                            return true;
                        }
                        if(cur_edt.get(curAffect).getHoraire().getEnd().isAfter(horaireJob.getBeg()))
                            debut_creneau_possible = cur_edt.get(curAffect).getHoraire().getEnd();
                        else
                            debut_creneau_possible = horaireJob.getBeg();
                        if(horaire_fin_creneau.isBefore(horaireJob.getEnd()))
                            fin_creneau_possible = horaire_fin_creneau;
                        else
                            fin_creneau_possible = horaireJob.getEnd();
                        curAffect++;
                    }
                if (!(fin_creneau_possible.isBefore(debut_creneau_possible))) {
                    //on teste si le job rentre dans le créneau (donc déplacement du dernier lieu + temps du job < fin du créneau)
                    rentre_creneau = (debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree())).isBefore(fin_creneau_possible);
                    Location nextLocation;
                    //maintenant on vérifie la cohérence pour le job suivant
                    if (curAffect >= cur_edt.size() - 1) {
                        //si c'était le dernier job de l'EDT : l'employé doit rentrer chez lui
                        nextLocation = e.getLocbeg();
                        int curHoraireEmploye = i;
                        Instant curtime = debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree());
                        //curtime va être égal au temps où on en est; la fin du job qu'on vient d'insérer pour l'instant puis le début du créneau horaire
                        //suivant de l'employé
                        while (curHoraireEmploye < horaireEmploye.size()) {
                            ok_next_job = ok_next_job || (curtime.plusSeconds(jobLocation.timeToGoTo(nextLocation, 60)).isBefore(horaireEmploye.get(curHoraireEmploye).getEnd()));
                            curHoraireEmploye++;
                            if (curHoraireEmploye < horaireEmploye.size())
                                curtime = horaireEmploye.get(curHoraireEmploye).getBeg();
                        }
                    } else {
                        //sinon : il y a une autre affectation après
                        nextLocation = cur_edt.get(curAffect + 1).getTravail().getLoc();
                        int curHoraireEmploye = i;
                        //même idée que précédemment
                        Instant curtime = debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree());
                        while (curHoraireEmploye < horaireEmploye.size() && cur_edt.get(curAffect + 1).getHoraire().getBeg().isAfter(horaireEmploye.get(curHoraireEmploye).getEnd())) {
                            curHoraireEmploye++;
                            curtime = horaireEmploye.get(curHoraireEmploye).getBeg();
                        }
                        ok_next_job = curtime.plusSeconds(jobLocation.timeToGoTo(nextLocation, 60)).isBefore(cur_edt.get(curAffect + 1).getHoraire().getBeg());
                    }
                }
                if (rentre_creneau && ok_next_job) {
                    //gagné
                    //il faut insérer le job
                    TimeInterval jobdone = new TimeInterval(debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)), debut_creneau_possible.plusSeconds(lastLocation.timeToGoTo(jobLocation, 60)).plusSeconds(j.getDuree()));
                    Affectation temp = new Affectation(jobdone, j);
                    solution.get(indiceEmp).addAffectation(temp);
                    return true;
                }

            }
        }
        return false;
    }

    public Metah(WsrModel m){
        this.modele = m ;

    }

    public void solve() {
        Heuristique gloutonned = new Heuristique(modele); //On appelle l'heuristique gloutonne une fois pour obtenir une solution
        gloutonned.affectAllJobs();
        List<Employee> employelist = modele.getLemploye();
        solution = gloutonned.getSolution();
        List<Boolean> tabou = new ArrayList<Boolean>();//pour checker si on a déjà réaffecté ce job
        for(int t = 0; t < modele.getJobs().size(); t++) {
            tabou.add(false);
        }

        /*for(int i = 0; i < modele.getJobs().size(); i++){
            System.out.println("Job"+modele.getJobs().get(i).getId());
            System.out.println("Horaire voulu"+modele.getJobs().get(i).getHorairevoulu());
            System.out.println("Horaire nécessaire"+modele.getJobs().get(i).getHorairenec());
            System.out.println("Durée du job"+modele.getJobs().get(i).getDuree());
        }

        for(int j = 0; j < modele.getLemploye().size(); j++){
            System.out.println("Employé"+j);
            System.out.println("Horaires dispo");
            for(int k = 0; k < modele.getLemploye().get(j).getHoraires().size(); k++){
                System.out.println(modele.getLemploye().get(j).getHoraires().get(k));
            }
        }*/

        //solution.sort((o1,o2) -> {if (o1.getAffectation().size() > o2.getAffectation().size()) return 1; else return -1;});//On met les employés qui ont le plus d'affectations en premier


        for(int i = 0; i < employelist.size() - 1; i++) { //On itère sur tous les employés
            List<TimeInterval> timetravel_cur = solution.get(employelist.get(i).getId()).getTimeTravel();
            List<Affectation> affect_cur = solution.get(employelist.get(i).getId()).getAffectation();
            Affectation toimprove;
            TimeInterval toimprove_origintime;
            TimeInterval origin_nexttime;
            boolean wasonlyjob = false;
            boolean waslastjob = false;
            int cur_affect = 0; //Pour savoir où on doit piocher l'affectation qu'on va essayer d'améliorer
            for(int j = 0; j < affect_cur.size(); j++){//Pour toutes les affectations de l'employé i
                if(tabou.get(affect_cur.get(cur_affect).getTravail().getId())){
                    cur_affect++;
                }
                else {
                    tabou.set(affect_cur.get(cur_affect).getTravail().getId(),true);
                    Affectation curaff = affect_cur.get(cur_affect);
                    toimprove = curaff;
                    TimeInterval curtim = timetravel_cur.get(cur_affect);
                    toimprove_origintime = curtim;
                    solution.get(employelist.get(i).getId()).getAffectation().remove(cur_affect);
                    solution.get(employelist.get(i).getId()).getTimeTravel().remove(cur_affect); //On supprime l'affectation de l'emploi du temps
                    //Maintenant il faut updater le timetravel pour l'affectation i+1
                    //2 cas : si c'était la seule affectation : plus de time travel
                    //sinon : on met à jour le timetravel en récupérant la position précédente de l'employé i
                    if (solution.get(employelist.get(i).getId()).getAffectation().isEmpty()) {
                        wasonlyjob = true;
                        origin_nexttime = solution.get(employelist.get(i).getId()).getTimeTravel().get(0);
                        solution.get(employelist.get(i).getId()).setTimeTravel(new ArrayList<TimeInterval>());
                    }
                    else {
                        origin_nexttime = solution.get(employelist.get(i).getId()).getTimeTravel().get(cur_affect);
                        Location lastLocation;
                        Instant lastInstant;
                        List<Affectation> empAffectations = solution.get(employelist.get(i).getId()).getAffectation();
                        if (cur_affect == 0) {//Si employé n'a pas encore travaillé
                            //lastLocation = solution.get(i).getEmploye().getLocbeg();
                            Affectation secondaff = empAffectations.get(cur_affect);
                            solution.get(employelist.get(i).getId()).getAffectation().remove(cur_affect);
                            solution.get(employelist.get(i).getId()).getTimeTravel().remove(cur_affect);
                            solution.get(employelist.get(i).getId()).addAffectation(secondaff);
                        } else { //Si employé a deja travaillé
                            if (cur_affect == solution.get(employelist.get(i).getId()).getAffectation().size()) {
                                waslastjob = true;
                                Affectation prevaff = empAffectations.get(cur_affect-1);
                                solution.get(employelist.get(i).getId()).getAffectation().remove(cur_affect-1);
                                solution.get(employelist.get(i).getId()).getTimeTravel().remove(cur_affect-1);
                                solution.get(employelist.get(i).getId()).addAffectation(prevaff);

                            } else {
                                Affectation secondaff = empAffectations.get(cur_affect);
                                solution.get(employelist.get(i).getId()).getAffectation().remove(cur_affect);
                                solution.get(employelist.get(i).getId()).getTimeTravel().remove(cur_affect);
                                solution.get(employelist.get(i).getId()).addAffectation(secondaff);
                            }


                         /*   Affectation lastAffectation = empAffectations.get(cur_affect - 1);
                            lastLocation = lastAffectation.getTravail().getLoc();
                            lastInstant = lastAffectation.getHoraire().getEnd();
                            Location nextLocation;
                            if (cur_affect == solution.get(employelist.get(i).getId()).getAffectation().size()) {
                                waslastjob = true;
                                nextLocation = solution.get(employelist.get(i).getId()).getEmploye().getLocend();
                            } else {
                                nextLocation = solution.get(employelist.get(i).getId()).getAffectation().get(cur_affect).getTravail().getLoc();
                            }
                            int timetravelcurrent = nextLocation.timeToGoTo(lastLocation, 60);
                            solution.get(employelist.get(i).getId()).getTimeTravel().get(cur_affect).setBeg(lastInstant);
                            solution.get(employelist.get(i).getId()).getTimeTravel().get(cur_affect).setEnd(lastInstant.plusSeconds(timetravelcurrent));
                        */}
                    }
                    boolean affected = false;
                    //on va essayer de réinsérer le job
                    List<Employee> copieemploye = new ArrayList<Employee>();
                    for(int z = 0; z < employelist.size(); z++){
                        if(employelist.get(z).getAbL().get(0).compatibleSkills(toimprove.getTravail().getAbL(),employelist.get(z).getAbL()))
                            copieemploye.add(employelist.get(z));
                    }
                    copieemploye.sort((o1, o2) -> {if(solution.get(o1.getId()).getAffectation().size() > solution.get(o2.getId()).getAffectation().size()) return 1; else return -1; });


                    for(int k = 0; k < copieemploye.size(); k++){
                        if(copieemploye.get(k).getId() != solution.get(employelist.get(i).getId()).getEmploye().getId() && !affected){
                            //Pour l'instant on évite de réaffecter au même employé car cela peut causer des problèmes (TODO améliorable)
                            affected = affectToEmploye_horairevoulu(toimprove.getTravail(),solution.get(copieemploye.get(k).getId()).getEmploye(),copieemploye.get(k).getId());
                            //affected = affectToEmploye_horairevoulu(toimprove.getTravail(),solution.get(k).getEmploye(),k);
                        }
                    }
                    if(affected){
                    }
                    if(!affected){
                        //On a pas réussi à réinsérer avec les horaires idéales : alors on remet le cas de base
                        //solution.get(employelist.get(i).getId()).addAffectation(toimprove);
                        solution.get(employelist.get(i).getId()).getAffectation().add(cur_affect,toimprove);
                        solution.get(employelist.get(i).getId()).getTimeTravel().add(cur_affect,toimprove_origintime);
                        if(wasonlyjob){
                            solution.get(employelist.get(i).getId()).getTimeTravel().add(origin_nexttime);
                        }
                        else {
                            /*if(cur_affect + 1 < solution.get(employelist.get(i).getId()).getAffectation().size()){
                                Affectation temp = solution.get(employelist.get(i).getId()).getAffectation().get(cur_affect+1);
                                solution.get(employelist.get(i).getId()).getAffectation().remove(cur_affect+1);
                                solution.get(employelist.get(i).getId()).getTimeTravel().remove(cur_affect+1);
                                solution.get(employelist.get(i).getId()).addAffectation(temp);
                            }*/
                            solution.get(employelist.get(i).getId()).getTimeTravel().get(cur_affect + 1).setBeg(origin_nexttime.getBeg());
                            solution.get(employelist.get(i).getId()).getTimeTravel().get(cur_affect + 1).setEnd(origin_nexttime.getEnd());
                        }
                        cur_affect++;
                    }
                }
            }
        }
    }
}
