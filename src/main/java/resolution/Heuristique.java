package resolution;

import modele.*;
import java.time.Instant;
import java.util.*;

public class Heuristique {
    private WsrModel modele;
    private List<Edt> solution;

    public WsrModel getModele() {
        return modele;
    }

    public List<Edt> getSolution() {
        return solution;
    }

    public Heuristique(WsrModel modele) {
        this.modele = modele;
        this.solution = new ArrayList<Edt>();
         for (int i = 0; i < modele.getLemploye().size(); i++) {
         solution.add(new Edt(modele.getLemploye().get(i)));
         }
    }
    public Location getLocOfTheLastAffectation(List<Affectation> affectationList, Employee e){
     if (affectationList.isEmpty()){
         return e.getLocbeg();
     }
     else
         return affectationList.get(affectationList.size()-1).getTravail().getLoc();
    }

    public boolean affectToEmploye(Job j, Employee e,int indiceEmp){
        //Assert : Employé J = Employé de l'EDT J

        Location lastLocation;
        Instant lastInstant;
        TimeInterval horaireNec = j.getHorairenec();
        List<Affectation> empAffectations = solution.get(indiceEmp).getAffectation();

        if (empAffectations.isEmpty()) {//Si employé j n'as pas encore travaillé
            lastLocation = e.getLocbeg();
            lastInstant = e.getHoraires().get(0).getBeg();
        }
        else{ //Si employé j as deja travaillé
            Affectation lastAffectation = empAffectations.get(empAffectations.size()-1);
            lastLocation = lastAffectation.getTravail().getLoc();
            lastInstant = lastAffectation.getHoraire().getEnd();
        }
        TimeInterval horaireCurrentEmployee = e.getHoraires().get(0);//On cherche la fin du creneau horaire de l'employé
        int horaireCurrentEmployeeIndice = 0;
        for (int k = 0; k < e.getHoraires().size(); k++) {
            if (e.getHoraires().get(k).getEnd().isAfter(lastInstant)) {
                horaireCurrentEmployee = e.getHoraires().get(k);
                horaireCurrentEmployeeIndice = k;
                break;
            }
        }
        boolean lastHoraire = false;
        lastInstant = lastInstant.plusSeconds(lastLocation.timeToGoTo(j.getLoc(),60));

        if(lastInstant.isBefore(horaireNec.getBeg())){//Si le debut de la tache commence apres que l'employe puisse arriver a destination
            lastInstant = horaireNec.getBeg();
            //if(j.getId()==26) System.out.println(horaireNec);
        }
        Instant startingJob = lastInstant;
        lastInstant = lastInstant.plusSeconds(j.getDuree());
        if (horaireCurrentEmployee == e.getHoraires().get(e.getHoraires().size()-1)) {// Si on est sur le dernier creneau
            //Alors on compte aussi le temps de rentrer a la maison
            lastInstant = lastInstant.plusSeconds(j.getLoc().timeToGoTo(e.getLocend(),60));
            lastHoraire = true;
        }



        if(lastInstant.isBefore(horaireCurrentEmployee.getEnd()) && lastInstant.isBefore(horaireNec.getEnd())){
            //Si ca rentre avant la fin de l'horaire et que on peut finir la tache dans le temps imparti
            TimeInterval horaire =  new TimeInterval(startingJob,startingJob.plusSeconds(j.getDuree()));
            Affectation a = new Affectation(horaire,j);
            solution.get(indiceEmp).addAffectation(a);
            return true;
        }
        else if(!lastHoraire && lastInstant.isBefore(horaireNec.getEnd())){
            // Affecter a l'horaire suivante
            startingJob = e.getHoraires().get(horaireCurrentEmployeeIndice+1).getBeg();
            int traveltime = j.getLoc().timeToGoTo(
                    getLocOfTheLastAffectation(solution.get(indiceEmp).getAffectation(),e), 60);
            for (int i = horaireCurrentEmployeeIndice+1; i < e.getHoraires().size(); i++) {
                //iterer sur les planches horaires
                startingJob = e.getHoraires().get(i).getBeg().plusSeconds(traveltime);
                TimeInterval horaire;
                if (i == e.getHoraires().size()-1) {
                    if (startingJob.plusSeconds(j.getDuree()).plusSeconds(j.getLoc().timeToGoTo(e.getLocend(),60)).isBefore(e.getHoraires().get(i).getEnd()))//Si ca rentre dans la plage i
                    {
                        horaire =  new TimeInterval(startingJob,startingJob.plusSeconds(j.getDuree()));
                        Affectation a = new Affectation(horaire,j);
                        solution.get(indiceEmp).addAffectation(a);
                        return true;
                    }
                }
                else if(startingJob.plusSeconds(j.getDuree()).isBefore(e.getHoraires().get(i).getEnd()))//Si ca rentre dans la plage i
                {
                    horaire =  new TimeInterval(startingJob,startingJob.plusSeconds(j.getDuree()));
                    Affectation a = new Affectation(horaire,j);
                    solution.get(indiceEmp).addAffectation(a);
                    return true;
                }
            }
        }

        return false;
    }

    public boolean affectTheJob(Job j){
        List<Employee> employelist = modele.getLemploye();
        for (int i = 0; i < employelist.size(); i++) {
            if( employelist.get(i).getAbL().get(0).compatibleSkills(j.getAbL(),employelist.get(i).getAbL()) &&
                    (affectToEmploye(j, employelist.get(i),i)))
                return true;
        }
        return false;
    }


    public void affectAllJobs() {
        List<Job> joblist = modele.getJobs();
        int jobsize = modele.getJobs().size();
        boolean affecte = false;
        joblist.sort((o1,o2)->o1.getHorairenec().getEnd().plusSeconds(o1.getDuree()).compareTo(o2.getHorairenec().getEnd().plusSeconds(o2.getDuree())));

        for (int i = 0; i < joblist.size() - 1; i++) {//Pour tout les jobs

            affecte = affectTheJob(joblist.get(i));
            if (affecte) jobsize--;
        }
        //System.out.println("Nombre de job non affecté :"+ jobsize);
        //System.out.println("Nombre de job affecté :"+(modele.getJobs().size()-jobsize));

    }


}