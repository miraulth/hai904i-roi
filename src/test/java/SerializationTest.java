import modele.*;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;


public class SerializationTest {

    @Test
    void testSerilizationModel() throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper  mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        List<Employee> employeelist = Main.InstanceGeneratorEmployee(5,5);
        List<Job> joblist = Main.InstanceGeneratorJobs(5,3);
        WsrModel m = new WsrModel(joblist,employeelist);
        Main.writeJsonModel(m,new File("files/WrsModelTestSerialization.json"));

    }
    @Test
    void testDeserilizationModel() throws JsonGenerationException, JsonMappingException, IOException {

        WsrModel model = Main.readJsonModel(new File("files/WrsModelTestSerialization.json"));
    }
    @Test
    void testSerilizationEdt() throws JsonGenerationException, JsonMappingException, IOException {

        Job job = Main.InstanceGeneratorJobs(1,1).get(0);
        TimeInterval ti = new TimeInterval(Instant.now(),Instant.now().plusSeconds(3600));
        Affectation aff = new Affectation(ti,job);
        Employee employee = Main.InstanceGeneratorEmployee(1,5).get(0);
        Edt edt = new Edt(employee,aff,ti);
        Main.writeJsonEdt(edt, new File("files/EdtTestSerialization.json"));

    }
    @Test
    void testDeserilizationEdt() throws JsonGenerationException, JsonMappingException, IOException {
        Edt edt = Main.readJsonEdt(new File("files/EdtTestSerialization.json"));
    }
}
