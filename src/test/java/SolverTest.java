import solution.SolutionChecker;
import resolution.Heuristique;
import resolution.Metah;
import modele.Main;
import modele.WsrModel;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

class SolverTest {


    @Test
    void GluttonTestWeak() {
        File f = new File("files/Instance_10.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
            e.printStackTrace();
        }
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();
        boolean isSol = SolutionChecker.isSolution(solverGlouton.getSolution(),solverGlouton.getModele());
        if(isSol){
            System.out.println("La solution est valide");
        }else{
            System.out.println("La solution n'est pas valide");
        }


        for (int i = 0; i < solverGlouton.getSolution().size(); i++) {
            System.out.println(solverGlouton.getSolution().get(i));
        }
    }

    @Test
    void GluttonMetaTestWeak() {
        File f = new File("files/Instance_10.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
            e.printStackTrace();
        }
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();

        Metah metaheuristik = new Metah(model);
        metaheuristik.solve();

        boolean valide = SolutionChecker.isSolution(metaheuristik.getSolution(),metaheuristik.getModele());
        if(valide){
            System.out.println("La solution est valide");
        }else{
            System.out.println("La solution n'est pas valide");
        }


        for (int i = 0; i < solverGlouton.getSolution().size(); i++) {
            System.out.println("****** HEURISTIQUE ******");
            System.out.println(solverGlouton.getSolution().get(i));
            System.out.println("****** METAHEURISTIQUE ******");
            System.out.println(metaheuristik.getSolution().get(i));
            for(int j = 0; j < metaheuristik.getSolution().get(i).getAffectation().size(); j++){
                System.out.println("Affectation :"+metaheuristik.getSolution().get(i).getAffectation().get(j).getTravail());
            }
        }
    }

    @Test
    void GluttonTestMedium() {
        File f = new File("files/Instance_40.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
        }
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();
        boolean isSol = SolutionChecker.isSolution(solverGlouton.getSolution(),solverGlouton.getModele());
        if(isSol){
            System.out.println("La solution est valide");
        }else{
            System.out.println("La solution n'est pas valide");
        }


        for (int i = 0; i < solverGlouton.getSolution().size(); i++) {
            System.out.println(solverGlouton.getSolution().get(i));
        }
    }

    @Test
    void GluttonTestStrong() {
        File f = new File("files/Instance_100.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
        }
        Heuristique solverGlouton = new Heuristique(model);
        solverGlouton.affectAllJobs();
        boolean isSol = SolutionChecker.isSolution(solverGlouton.getSolution(),solverGlouton.getModele());
        if(isSol){
            System.out.println("La solution est valide");
        }else{
            System.out.println("La solution n'est pas valide");
        }


        for (int i = 0; i < solverGlouton.getSolution().size(); i++) {
            System.out.println(solverGlouton.getSolution().get(i));
        }
    }
}
