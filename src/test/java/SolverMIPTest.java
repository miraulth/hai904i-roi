

import com.skaggsm.ortools.OrToolsHelper;
import modele.Main;
import modele.WsrModel;
import org.junit.jupiter.api.Test;
import resolution.MIP;
import solution.SolutionChecker;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;


/*
 * Created by chloe in 2021
 */
class SolverMIPTest {
    static{
        // Somewhere before using OR-Tools classes
        OrToolsHelper.loadLibrary();
    }
    @Test
    void MIPTestWeak() {

        File f = new File("files/Instance_2.json");
        WsrModel model = null;
        try {
            model = Main.readJsonModel(f);
        } catch (IOException e) {
            System.err.println("Probleme avec le fichier");
            e.printStackTrace();
        }
        Instant beg = Instant.now().plusSeconds(999999999);
        Instant end = Instant.now().minusSeconds(999999999);
        for(int i=0;i<model.getJobs().size();i++){
            if(model.getJobs().get(i).getHorairenec().getBeg().getEpochSecond() < beg.getEpochSecond()){
                beg = model.getJobs().get(i).getHorairenec().getBeg();
            }
            if(model.getJobs().get(i).getHorairenec().getEnd().getEpochSecond() > end.getEpochSecond()){
                end = model.getJobs().get(i).getHorairenec().getEnd();
            }
        }
        for(int i=0;i<model.getLemploye().size();i++){
            if(model.getLemploye().get(i).getHoraires().get(0).getBeg().getEpochSecond() < beg.getEpochSecond()){
                beg = model.getJobs().get(i).getHorairenec().getBeg();
            }
            if(model.getLemploye().get(i).getHoraires().get(model.getLemploye().get(i).getHoraires().size()-1).getEnd().getEpochSecond() > end.getEpochSecond()){
                end = model.getLemploye().get(i).getHoraires().get(0).getEnd();
            }
        }
        MIP solverMIP = new MIP(model, beg, end,10000,60,999999,0.99f,0.0001f,0.0001f,0.0001f);

        Duration d = Duration.ofSeconds(20);
        boolean result = solverMIP.solveMIP(d);

        System.out.println("Résultat du MIP = "+result);
        boolean isSol = SolutionChecker.isSolution(solverMIP.getSolution(),solverMIP.getModel());

        if(!isSol && result){
            System.out.println("AÏE !");
        }
        if(isSol){
            System.out.println("La solution est valide");
        }else{
            System.out.println("La solution n'est pas valide");
        }
        for (int i = 0; i < solverMIP.getSolution().size(); i++) {
            System.out.println(solverMIP.getSolution().get(i));
        }
    }
}
