Les tests se font dans le fichier SolverTest.java.
GluttonTestWeak teste l'heuristique gloutonne sur la plus petite instance du
problème, et renvoie l'affectation.
GluttonTestMedium et GluttonTestHard font de même sur des plus grosses
instances.
GluttonMetaTestWeak teste l'heuristique gloutonne ainsi que la metaheuristique
sur les plus petites instances, et renvoie les 2 affectations.

Pour tester les algorithmes sur n'importe quelle instance, vous pouvez modifier
la ligne File f = new File("files/YOURFILEHERE.json");
dans n'importe quel fonction de test.
